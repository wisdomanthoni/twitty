@extends('layouts.app')

@section('content')

     
 <aside class="col-md-2" style="overflow: scroll;" > 
       <h3>Users</h3>
        <ul>
               @foreach($members as $member)
                  <li>
                     <div class="panel">
                         <a href="{{ url('/' . $member->username) }}">
                            <h6><strong>{{ $member->name or 'Full Name' }}</strong></h6>
                        </a>
                        <a href="{{ url('/' . $member->username) }}">
                            <small>&#64;{{ $member->username or 'username' }}</small>
                        </a>
                  </div>
                  </li>
             @endforeach
       </ul>
</aside>
  <div class="col-md-10">
     <div class="row">
           @if (session('status'))
                   <div class="alert alert-success fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <p>  <strong> {{ session('status') }}</strong>  </p>
            </div>
       @endif


        <div class="col-md-8 col-md-offset-2">

             <div class=" panel panel-body">
                    <form class="form" method="POST" action="/twt">
                        {{ csrf_field() }}
                        <div class="row">
                          <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <div class="col-md-9">
                              
                                <textarea class="form-control"  value="{{ old('post') }}" name="body" id="body" cols="7" rows="3"  placeholder=" Yell out to the world" required autofocus></textarea>

                                @if ($errors->has('body'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         
                        <div class="form-group">
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary btn-lg">
                                   Post
                                </button>
                                
                            </div>
                        </div>
                      </div>  
                    </form>
                </div>
            <div class="panel panel-default">
                <div class="panel-heading">Timeline</div>

                  {{--   <div class="panel-body" id="list-1" v-infinite-scroll="loadMore" infinite-scroll-disabled="busy" infinite-scroll-distance="10">
                            <a href="#" class="list-group-item" v-for="tweet in items">
                                <h4 class="list-group-item-heading">@{{ tweet.body }}</h4>
                                <p class="list-group-item-text">@{{ tweet.created_at }}</p>
                            </a>
                        </div> --}}
                        <div class="panel-body">
                        @forelse ($user->timeline() as $tweet)
                            <div class="list-group-item">
                                <p style="border: 1px solid; border-radius: 2px;">
                                    Posted by <a href="{{url('/'. $tweet->user->username)}}">{{ $tweet->user->username}}</a> {{ $tweet->created_at->diffForHumans() }}
                                </p>
                                <hr>
                                <h4 class="list-group-item-heading lead">{{ $tweet->body }}</h4>
                                 
                                 
                            </div>
                            <br>
                        @empty
                            <p>No tweet</p>
                        @endforelse
                    </div>
               
                </div>
            </div>
        </div>
   </div>
@endsection

{{-- @section('script')
<script src="https://unpkg.com/vue-resource@1.2.0/dist/vue-resource.min.js"></script>
<script src="https://unpkg.com/vue-infinite-scroll@2.0.0"></script>
<script type="text/javascript">
    var page = 1;
    new Vue({
      el: '#list-1',
      data: {
        page: 1,
        items: [],
        busy: false
      },
      methods: {
        loadMore: function() {
            this.busy = true;
            var url = '/timeline' + (this.page > 1 ? '?page=' + this.page : '');
            this.$http.get(url)
            .then(response => {
                var data = response.body;
                // Push the response data into items
                for (var i = 0, j = data.length; i < j; i++) {
                  this.items.push(data[i]);
                }
                // If the response data is empty,
                // disable the infinite-scroll
                this.busy = (j < 1);
                // Increase the page number
                this.page++;
            });
        }
      }
    });
</script>
@endsection --}}
