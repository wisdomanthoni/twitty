<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Tweet extends Model
{
    protected $fillable = [
        'user_id', 'body',
    ];

 
    public function user(){
      return 	$this->belongsTo('\App\User');
    }
}
